# livreblanc-interprofessionnalite

Les multiples travaux engagés dans le cadre du programme Interprofessionnalité ont été documentés dans un livre blanc à l’issue du cycle afin de donner les clés à tous.

Voir https://openlaw.fr/travaux/cycles/interprofessionnalite-simuler-une-spe